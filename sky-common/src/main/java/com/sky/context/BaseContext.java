package com.sky.context;

/**
 * ThreadLocal工具类
 */
public class BaseContext {

    //创建了一个线程局部变量对象
    public static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    /**
     * 存储变量（empid）
     *
     * @param id
     */
    public static void setCurrentId(Long id) {
        threadLocal.set(id);
    }

    /**
     * 获取变量（empid）
     *
     * @return
     */
    public static Long getCurrentId() {
        return threadLocal.get();
    }

    /**
     * 删除变量（员工ID）
     */

    public static void removeCurrentId() {
        threadLocal.remove();
    }

}
