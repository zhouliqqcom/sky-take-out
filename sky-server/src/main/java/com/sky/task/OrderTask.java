package com.sky.task;

import com.sky.constant.MessageConstant;
import com.sky.entity.Orders;
import com.sky.mapper.OrderMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @author Lee
 * @created 2024/5/14
 * @describtion
 */
@Component
@Slf4j
public class OrderTask {

    @Autowired
    private OrderMapper orderMapper;

    /**
     * 用户下单：
     * 1.下单后未支付，订单一直处于'待支付'的状态
     * 2.用户收货之后，管理端忘记点击已完成，订单一直处于'待派送'的状态
     * <p>
     * 解决方案：
     * 定时任务：
     * 订单超时处理的定时任务：
     * 处理派送中的订单任务:
     */

//    @Scheduled(cron = "0/5 * * * * ?")//每隔五秒执行一次
    @Scheduled(cron = "* * 8 * * ?")//每隔8小时执行一次
    public void orderTimeoutProcessing() {
        log.info("处理支付超时订单：{}", new Date());
        System.out.println("orderTimeoutProcessing  我执行了");

        //查询所有的超时订单，把每个状态改成已取消
        LocalDateTime now = LocalDateTime.now();
        //time是系统当前时间减去15分钟后的时间
        LocalDateTime time = now.plusMinutes(-15);
        //查询所有待支付订单
        List<Orders> ordersList = orderMapper.getOrederByStatusAndTime(Orders.PENDING_PAYMENT, time);

        //
        if (ordersList != null && !ordersList.isEmpty()) {

            //方法一：增强for循环
            for (Orders orders : ordersList) {
                orders.setStatus(Orders.CANCELLED);
                orders.setCancelReason(MessageConstant.AUTOMATIC_CANCELLATION_OF_PAYMENT_TIMEOUT);
                orders.setCancelTime(LocalDateTime.now());
                orderMapper.update(orders);
            }
           /* //方法2:steam流
            ordersList.stream().forEach(orders -> {
                orders.setStatus(Orders.CANCELLED);
                orders.setCancelReason(MessageConstant.AUTOMATIC_CANCELLATION_OF_PAYMENT_TIMEOUT);
                orders.setCancelTime(LocalDateTime.now());
                orderMapper.update(orders);
            });*/
        }

    }

    @Scheduled(cron = "0 59 23 * * ?")//每隔8小时执行一次
//    @Scheduled(cron = "0/5 * * * * ?")//每隔8小时执行一次
    public void processDeliveryOrder() {
        log.info("处理派送中的订单：{}", new Date());
        System.out.println(" 处理派送中的订单...... 我执行了");
        //查询所有的超时派送中订单，把每个状态改成已完成
        LocalDateTime now = LocalDateTime.now();
        //time是系统当前时间减去60分钟后的时间
        LocalDateTime time = now.plusMinutes(-60);

        List<Orders> ordersList = orderMapper.getOrederByStatusAndTime(Orders.DELIVERY_IN_PROGRESS, time);

        if (ordersList != null && !ordersList.isEmpty()) {
         /*   //方法一：增强for循环
            for (Orders orders : ordersList) {
                orders.setStatus(Orders.COMPLETED);
                orders.setDeliveryTime(LocalDateTime.now());
                orderMapper.update(orders);
            }*/

            ordersList.forEach(orders -> {
                orders.setStatus(Orders.COMPLETED);
                orders.setDeliveryTime(LocalDateTime.now());
                orderMapper.update(orders);
            });
        }
    }


}
