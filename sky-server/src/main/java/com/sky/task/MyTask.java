package com.sky.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author Lee
 * @created 2024/5/14
 * @describtion
 */
//@Component
@Slf4j
public class MyTask {
    /**
     * 测试定时任务
     */


    //所谓的定时任务，其实本质就是按照我们约定的时间去执行某个方法
    //@Scheduled(cron = "0/5 * * * * ?")
    public void task1() {
        LocalDateTime now = LocalDateTime.now();
        System.out.println("task1: " + now);
        System.out.println("我执行了");
    }


}
