package com.sky.service;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.result.PageResult;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;

import java.util.List;

/**
 * @author Lee
 * @created 2024/5/4
 * @describtion
 */
public interface SetmealService {

    /**
     * 新增套餐
     *
     * @param dto
     */
    void saveSetmeal(SetmealDTO dto);

    /**
     * 分页查询
     *
     * @param pageQueryDTO
     * @return
     */
    PageResult page(SetmealPageQueryDTO pageQueryDTO);

    /**
     * 根据ID查询套餐
     *
     * @param id
     * @return
     */
    SetmealVO getByIdSetmeal(Long id);

    /**
     * 批量删除套餐
     *
     * @param ids
     */
    void delSetmeal(List<Long> ids);

    /**
     * 修改套餐
     *
     * @param dto
     */
    void update(SetmealDTO dto);

    /**
     * 套餐起售和停售
     *
     * @param status
     * @param id
     */
    void startsAndDiscontinues(Integer status, Long id);

    /**
     * 条件查询
     *
     * @param setmeal
     * @return
     */
    List<Setmeal> list(Setmeal setmeal);

    /**
     * 根据id查询菜品选项
     * @param id
     * @return
     */
    List<DishItemVO> getDishItemById(Long id);
}
