package com.sky.service;

import com.sky.dto.UserLoginDTO;
import com.sky.entity.User;

/**
 * @author Lee
 * @created 2024/5/7
 * @describtion
 */
public interface UserService {

//    User login(UserLoginDTO dto);

    User wxLogin(UserLoginDTO userLoginDTO);
}
