package com.sky.service;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;

import java.util.List;

/**
 * @author Lee
 * @created 2024/5/9
 * @describtion
 */
public interface ShoppingCartService {
    /**
     * 添加购物车
     *
     * @param dto
     */
    void saveCart(ShoppingCartDTO dto);

    /**
     * 查询购物车
     *
     * @return
     */
    List<ShoppingCart> list();

    /**
     * 清空购物车
     */
    void clean();

    /**
     * 删除购物车中一个商品
     * @param dto
     */
    void subShoppingCart(ShoppingCartDTO dto);
}
