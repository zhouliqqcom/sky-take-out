package com.sky.service;

import com.sky.dto.*;
import com.sky.result.PageResult;
import com.sky.vo.OrderPaymentVO;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;

/**
 * @author Lee
 * @created 2024/5/10
 * @describtion
 */
public interface OrderService {
    /**
     * 用户下单
     *
     * @param dto
     * @return
     */
    OrderSubmitVO submitOrder(OrdersSubmitDTO dto);

    /**
     * 订单支付
     *
     * @param ordersPaymentDTO
     * @return
     */
    OrderPaymentVO payment(OrdersPaymentDTO ordersPaymentDTO) throws Exception;

    /**
     * 支付成功，修改订单状态
     *
     * @param outTradeNo
     */
    void paySuccess(String outTradeNo);

    /**
     * 订单搜索
     *
     * @param dto
     * @return
     */
    PageResult conditionSearch(OrdersPageQueryDTO dto);

    /**
     * 订单查询
     *
     * @param id
     * @return
     */
    OrderVO details(Long id);

    /**
     * 接单
     *
     * @param dto
     */
    void confirm(OrdersConfirmDTO dto);

    /**
     * 拒单
     *
     * @param dto
     */

    void rejection(OrdersRejectionDTO dto) throws Exception;

    /**
     * 取消订单
     *
     * @param dto
     */
    void cancel(OrdersRejectionDTO dto) throws Exception;

    /**
     * 派送订单
     *
     * @param id
     */
    void delivery(Long id);

    /**
     * 完成订单
     *
     * @param id
     */
    void complete(Long id);

    /**
     * 各个状态的订单数量统计
     *
     * @return
     */

    OrderStatisticsVO statistics();

    /**
     * C端-历史订单查询
     *
     * @param page
     * @param pageSize
     * @param status
     * @return
     */
    PageResult pageQuery4User(int page, int pageSize, Integer status);

    /**
     * 用户取消订单
     *
     * @param id
     */
    void cancelById(Long id) throws Exception;

    /**
     * 再来一单
     *
     * @param id
     */
    void reptition(Long id);

    /**
     * 订单催单
     * @param id
     */
    void reminder(Long id);
}
