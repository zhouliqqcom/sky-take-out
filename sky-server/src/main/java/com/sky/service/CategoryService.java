package com.sky.service;

import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.result.PageResult;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lee
 * @created 2024/5/2
 * @describtion
 */

public interface CategoryService {

    /**
     * 分类分页查询
     *
     * @param dto
     * @return
     */
    PageResult page(CategoryPageQueryDTO dto);

    /**
     * 新增分类
     *
     * @param dto
     */
    void save(CategoryDTO dto);

    /**
     * 分类的启用和禁用
     *
     * @param status
     * @param id
     */
    void enableDisable(Integer status, Long id);

    /**
     * 根据类型查询分类
     *
     * @param type 类型
     * @return
     */
    List<Category> typeQueryList(Integer type);

    /**
     * 修改分类
     *
     * @param dto
     */
    void update(CategoryDTO dto);

    /**
     * 根据分类ID删除
     *
     * @param id
     */
    void delById(Long id);

}
