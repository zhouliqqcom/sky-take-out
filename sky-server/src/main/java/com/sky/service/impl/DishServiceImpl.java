package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.entity.Setmeal;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.DishFlavorMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetMealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lee
 * @created 2024/5/3
 * @describtion
 */
@Service
public class DishServiceImpl implements DishService {


    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private DishFlavorMapper dishFlavorMapper;

    @Autowired
    private SetMealDishMapper setMealDishMapper;

    @Autowired
    private SetmealMapper setmealMapper;


    /**
     * 新增菜品的方法
     *
     * @param dto
     */
    @Transactional
    @Override
    public void saveDish(DishDTO dto) {

        Dish dish = new Dish();
        BeanUtils.copyProperties(dto, dish);
        dishMapper.saveDish(dish);
        //代码只要能走到这里，就可以证明dish 这个对象中一定会有SQL所返回的主键ID
//        System.out.println(dish.getId() + "******************");

        //保存口味
        List<DishFlavor> flavors = dto.getFlavors();
        if (flavors != null && flavors.size() > 0) {
            for (DishFlavor flavor : flavors) {
                //调用口味表的插入方法
                flavor.setDishId(dish.getId());
            }
            //批量保口味表数据
            dishFlavorMapper.insertBatch(flavors);
        }
    }

    /**
     * 分页查询
     *
     * @param dto
     * @return
     */
    @Override
    public PageResult page(DishPageQueryDTO dto) {
        //启动分页查询插件
        PageHelper.startPage(dto.getPage(), dto.getPageSize());
        //分页的数据
        Page<DishVO> page = dishMapper.page(dto);
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 批量|单个删除菜品
     *
     * @param ids
     */
    @Transactional
    @Override
    public void delecDishByIds(List<Long> ids) {

        //1.判断当前的菜品是否是起售状态，如果是，则抛出异常
        for (Long id : ids) {
            Dish dish = dishMapper.getDishById(id);
            if (dish.getStatus().equals(StatusConstant.ENABLE)) {
                throw new DeletionNotAllowedException(MessageConstant.DISH_ON_SALE);
            }
        }

        //2.判断当前的菜品是否被套餐所关联，如果被关联，则抛出异常
        List<Long> setMealIds = setMealDishMapper.getSetMealByDishId(ids);
        if (setMealIds != null && !setMealIds.isEmpty()) {
            throw new DeletionNotAllowedException(MessageConstant.CATEGORY_BE_RELATED_BY_SETMEAL);
        }


        //3.删除当前的菜品和对应菜品口味数据
        for (Long id : ids) {
            //删除菜品
            dishMapper.deleteDishById(id);

            //删除菜品的口味数据
            dishFlavorMapper.deleteDishFlavorByDishId(id);
        }

    }

    /**
     * 根据ID查询菜品
     *
     * @param id
     * @return
     */
    @Override
    public DishVO getByIdwithFlavor(Long id) {
        //根据菜品id查询
        Dish dish = dishMapper.getDishById(id);

        //根据口味id查询
        List<DishFlavor> dishFlavors = dishFlavorMapper.getFlavorById(id);

        DishVO dishVO = new DishVO();
        BeanUtils.copyProperties(dish, dishVO);
        dishVO.setFlavors(dishFlavors);

        return dishVO;
    }

    /**
     * 修改菜品信息
     *
     * @param dto
     */

    @Override
    public void updateWithFlavor(DishDTO dto) {
        Dish dish = new Dish();
        BeanUtils.copyProperties(dto, dish);
        //修改菜品表基本信息
        dishMapper.update(dish);

        //删除原来的口味信息
        dishFlavorMapper.deleteDishFlavorByDishId(dto.getId());

        //重新插入口味数据
        List<DishFlavor> flavors = dto.getFlavors();
        if (flavors != null && !flavors.isEmpty()) {
            flavors.forEach(dishFlavor -> {
                dishFlavor.setDishId(dto.getId());
            });
            //向口味表中插入多条数据
            dishFlavorMapper.insertBatch(flavors);
        }
    }

    /**
     * 菜品起售和停售
     *
     * @param status
     * @param id
     */
    @Override
    public void startsAndDiscontinues(Integer status, Long id) {
        Dish dish = Dish.builder().status(status).id(id).build();
        dishMapper.update(dish);


        //如果是停售操作，还需要将包含当前菜品的套餐也停售
        //TODO


        if (status.equals(StatusConstant.ENABLE)) {
            List<Long> dishIds = new ArrayList<>();
            dishIds.add(id);
            //根据获取菜品ID
            List<Long> setMealByDishIds = setMealDishMapper.getSetMealByDishId(dishIds);
            if (setMealByDishIds != null && !setMealByDishIds.isEmpty()) {
                for (Long setmealId : setMealByDishIds) {
                    Setmeal setmeal = Setmeal.builder()
                            .id(setmealId)
                            .status(StatusConstant.DISABLE).build();
                    setmealMapper.update(setmeal);
                }
            }
        }
    }

    /**
     * 根据分类id查询菜品
     *
     * @param categoryId
     * @return
     */
    @Override
    public List<Dish> list(Long categoryId, String name) {
        Dish dish = Dish.builder()
                .categoryId(categoryId)
                .name(name)
                .status(StatusConstant.ENABLE)
                .build();
        return dishMapper.list(dish);
    }

    /**
     * 条件查询菜品和口味
     *
     * @param dish
     * @return
     */
    @Override
    public List<DishVO> listWithFlavor(Dish dish) {
        List<Dish> list = dishMapper.list(dish);

        List<DishVO> dishVOList = new ArrayList<>();

        for (Dish d : list) {
            DishVO dishVO = new DishVO();
            BeanUtils.copyProperties(d, dishVO);
            List<DishFlavor> flavorById = dishFlavorMapper.getFlavorById(d.getId());
            dishVO.setFlavors(flavorById);
            dishVOList.add(dishVO);
        }
        return dishVOList;
    }


}
