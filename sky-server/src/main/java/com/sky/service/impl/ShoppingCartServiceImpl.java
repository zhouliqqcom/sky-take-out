package com.sky.service.impl;

import com.sky.context.BaseContext;
import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.ShoppingCart;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.ShoppingCartMapper;
import com.sky.service.ShoppingCartService;
import com.sky.vo.SetmealVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Lee
 * @created 2024/5/9
 * @describtion
 */
@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    private ShoppingCartMapper shoppingCartMapper;
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private SetmealMapper setmealMapper;

    /**
     * 添加购物车
     *
     * @param dto
     */
    @Override
    public void saveCart(ShoppingCartDTO dto) {
        ShoppingCart shoppingCart = new ShoppingCart();
        BeanUtils.copyProperties(dto, shoppingCart);
        //把购物车中的数据的数据添加到购物车表中
        //1.先获取当前的登录人的id,存到shoppingCart这个表
        Long userId = BaseContext.getCurrentId();
        shoppingCart.setUserId(userId);

        //2.根据这个shoppingCart去查询当前的商品是否存在
        //虽然用的是集合来接受，但是它只会存在两种情况，，一种是0，一种是1
        //因为复用性，-->将来如果需要查询购物车列表数据，我就不用在写sql了
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.list(shoppingCart);

        //3.如果存在,修改数量+1
        if (shoppingCartList != null && shoppingCartList.size() == 1) {
            ShoppingCart cart = shoppingCartList.get(0);
            Integer number = cart.getNumber();
            cart.setNumber(number + 1);
            shoppingCartMapper.updateById(cart);

        } else {
            //4.如果不存在，新增商品
            //还要继续判断当前是菜品的新增还是套餐的新增
            if (dto.getDishId() != null) {
                //新增的是菜品 ->dishid ->dish
                Dish dish = dishMapper.getDishById(dto.getDishId());
                shoppingCart.setName(dish.getName());
                shoppingCart.setAmount(dish.getPrice());
                shoppingCart.setImage(dish.getImage());
            } else {
                //新增的是套餐
                SetmealVO setmeal = setmealMapper.getByIdSetmeal(dto.getSetmealId());
                shoppingCart.setName(setmeal.getName());
                shoppingCart.setAmount(setmeal.getPrice());
                shoppingCart.setImage(setmeal.getImage());
            }
            shoppingCart.setNumber(1);
            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCartMapper.saveCart(shoppingCart);
        }
    }

    /**
     * 清空购物车
     * @return
     */
    @Override
    public List<ShoppingCart> list() {
//方法一：
//        ShoppingCart shoppingCart = ShoppingCart
//                .builder()
//                .userId(BaseContext.getCurrentId())
//                .build();
//方法二：
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setUserId(BaseContext.getCurrentId());
        return shoppingCartMapper.list(shoppingCart);
    }

    /**
     * 清空购物车
     */
    @Override
    public void clean() {
        Long userId = BaseContext.getCurrentId();
        shoppingCartMapper.deleteByUserId(userId);

    }

    /**
     * 删除商品
     *
     * @param dto
     */
    @Override
    public void subShoppingCart(ShoppingCartDTO dto) {
        ShoppingCart shoppingCart = new ShoppingCart();
        BeanUtils.copyProperties(dto, shoppingCart);
        shoppingCart.setUserId(BaseContext.getCurrentId());
        //设置查询条件，查询当前登录用户的购物车数据
        List<ShoppingCart> list = shoppingCartMapper.list(shoppingCart);
        // 判断list是否有数据
        if (list != null && !list.isEmpty()) {
            shoppingCart = list.get(0);
            Integer number = shoppingCart.getNumber();
            if (number == 1) {
                //当前商品在购物车的份数为1，直接删除当前记录
                shoppingCartMapper.deleteById(shoppingCart.getId());
            } else {
                //当前商品在购物车的份数不为1，修改份数即可
                shoppingCart.setNumber(shoppingCart.getNumber() - 1);
                shoppingCartMapper.updateById(shoppingCart);
            }
        }
    }
}
