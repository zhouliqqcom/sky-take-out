package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.anno.AutoFill;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.enumeration.OperationType;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.CategoryMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.CategoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Lee
 * @created 2024/5/2
 * @describtion
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private SetmealMapper setmealMapper;

    /**
     * 分类的启用和禁用
     *
     * @param status
     * @param id
     */
    @Override
    public void enableDisable(Integer status, Long id) {
        Category build = Category.builder()
                .status(status)
//                .updateTime(LocalDateTime.now())
//                .updateUser(BaseContext.getCurrentId())
                .id(id)
                .build();
        categoryMapper.update(build);

    }

    /**
     * 根据类型查询分类
     *
     * @param type 类型
     * @return
     */

    @Override
    public List<Category> typeQueryList(Integer type) {
        return categoryMapper.typeQueryList(type);
    }

    /**
     * 修改分类
     *
     * @param dto
     */
    @Override
    public void update(CategoryDTO dto) {
        Category category = new Category();
        BeanUtils.copyProperties(dto, category);
//        category.setUpdateTime(LocalDateTime.now());
//        category.setCreateUser(BaseContext.getCurrentId());
        categoryMapper.update(category);

    }

    /***
     *根据ID删除分类
     * @param id
     */
    @Override
    public void delById(Long id) {
        //查询当前分类是否关联了菜品，如果关联了就抛出业务异常
        Integer count = dishMapper.countByCategoryId(id);
        if (count > 0) {
            //当前分类下有菜品，不能删除
            throw new DeletionNotAllowedException(MessageConstant.CATEGORY_BE_RELATED_BY_DISH);
        }
        //查询当前分类是否关联了套餐，如果关联了就抛出业务异常
        count = setmealMapper.countByCategoryId(id);
        if (count > 0) {
            //当前分类下有菜品，不能删除
            throw new DeletionNotAllowedException(MessageConstant.CATEGORY_BE_RELATED_BY_SETMEAL);
        }
        categoryMapper.delById(id);

    }



    /**
     * 新增分类
     *
     * @param dto
     */

    @Override
    public void save(CategoryDTO dto) {
        Category category = Category.builder().build();
        BeanUtils.copyProperties(dto, category);
//        category.setCreateTime(LocalDateTime.now());
//        category.setCreateUser(BaseContext.getCurrentId());
//        category.setUpdateTime(LocalDateTime.now());
//        category.setUpdateUser(BaseContext.getCurrentId());
        category.setStatus(StatusConstant.DISABLE);
        categoryMapper.save(category);
    }

    /**
     * 分页分类查询
     *
     * @param dto
     * @return
     */
    @Override
    public PageResult page(CategoryPageQueryDTO dto) {
        PageHelper.startPage(dto.getPage(), dto.getPageSize());
        Page<Category> page = categoryMapper.page(dto);
        long total = page.getTotal();
        //将所有的分类的数据放到List集合中
        List<Category> result = page.getResult();
        return new PageResult(total, result);
    }





}
