package com.sky.service.impl;


import com.sky.dto.GoodsSalesDTO;
import com.sky.entity.Orders;
import com.sky.mapper.OrderMapper;
import com.sky.mapper.UserMapper;
import com.sky.service.ReportService;
import com.sky.service.WorkspaceService;
import com.sky.vo.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Lee
 * @created 2024/5/14
 * @describtion
 */
@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private WorkspaceService workspaceService;


    /**
     * 营业额数据统计
     *
     * @param begin
     * @param end
     * @return
     */
    @Override
    public TurnoverReportVO getTurnover(LocalDate begin, LocalDate end) {
        //1.计算出开始时间和结束时间范围之内的所有时间，使用List集合来存储多个时间
        ArrayList<LocalDate> dateList = new ArrayList<>();
        dateList.add(begin);
        while (!begin.equals(end)) {
            begin = begin.plusDays(1);
            dateList.add(begin);
        }
        //2.遍历集合，取出里面的每一个的日期，查出来每天的营业额
        ArrayList<Double> turnoverList = new ArrayList<>();
        //获取日营业额
        for (LocalDate date : dateList) {
            //获取开始时间和结束时间（将LocalDate转换成LocalDateTime）
            LocalDateTime beginTime = LocalDateTime.of(date, LocalTime.MIN);
            LocalDateTime endTime = LocalDateTime.of(date, LocalTime.MAX);
            //date表示每一天的日期
            HashMap<Object, Object> map = new HashMap<>();
            map.put("begin", beginTime);
            map.put("end", endTime);
            map.put("status", Orders.COMPLETED);

            //查询每一天的营业额
            Double turnover = orderMapper.sumByMap(map);

            if (turnover == null) {
                turnover = 0.0;
            }
            turnoverList.add(turnover);

        }
        //把两个集合全部变成String类型，中间使用逗号分隔
        //使用StringUtils工具类的join方法把这个集合里面的元素全部取出来，中间使用逗号分割，最后封装成一个String类型
        String strDateList = StringUtils.join(dateList, ",");
        String strturnoverList = StringUtils.join(turnoverList, ",");

        return TurnoverReportVO.builder()
                .dateList(strDateList)
                .turnoverList(strturnoverList)
                .build();
    }

    /**
     * 用户统计
     *
     * @param begin 开始时间
     * @param end   结束时间
     * @return UserReportVO
     */
    @Override
    public UserReportVO userStatistics(LocalDate begin, LocalDate end) {

        //1.计算出开始时间和结束时间范围之内的所有时间，使用List集合来存储多个时间
        ArrayList<LocalDate> dateList = new ArrayList<>();
        dateList.add(begin);
        while (!begin.equals(end)) {
            begin = begin.plusDays(1);
            dateList.add(begin);
        }
        //新增用户数
        List<Integer> newUserList = new ArrayList<>();
        //总用户数
        List<Integer> totalUserList = new ArrayList<>();

        for (LocalDate date : dateList) {
            LocalDateTime beginTime = LocalDateTime.of(date, LocalTime.MIN);
            LocalDateTime endTime = LocalDateTime.of(date, LocalTime.MAX);
            //新增用户数
            HashMap<Object, Object> map = new HashMap<>();
            map.put("begin", beginTime);
            map.put("end", endTime);
            Integer newUserCount = userMapper.countByMap(map);
            newUserList.add(newUserCount);
            //总用户数
            map = new HashMap<>();
            map.put("end", endTime);
            Integer totalUserCount = userMapper.countByMap(map);
            totalUserList.add(totalUserCount);

        }
        //组装返回数据
        String dateListStr = StringUtils.join(dateList, ",");
        String newUserListStr = StringUtils.join(newUserList, ",");
        String totalUserListStr = StringUtils.join(totalUserList, ",");

        return UserReportVO.builder()
                .dateList(dateListStr)
                .newUserList(newUserListStr)
                .totalUserList(totalUserListStr)
                .build();
    }

    /**
     * 订单数据统计
     *
     * @param begin 开始时间
     * @param end   结束时间
     * @return OrderReportVO
     */

    @Override
    public OrderReportVO orderStatistics(LocalDate begin, LocalDate end) {
        //1.计算出开始时间和结束时间范围之内的所有时间，使用List集合来存储多个时间
        ArrayList<LocalDate> dateList = new ArrayList<>();
        dateList.add(begin);
        while (!begin.equals(end)) {
            begin = begin.plusDays(1);
            dateList.add(begin);
        }
        //2.创建订单总数的集合和订单有效总数的集合
        List<Integer> orderCountList = new ArrayList<>();
        //总用户数
        List<Integer> theNumberOfActiveOrdersList = new ArrayList<>();

        //3.遍历集合，取出里面的每一个的日期，查出来每天的订单总数和订单有效总数
        for (LocalDate date : dateList) {
            //获取开始时间和结束时间（将LocalDate转换成LocalDateTime）
            LocalDateTime beginTime = LocalDateTime.of(date, LocalTime.MIN);
            LocalDateTime endTime = LocalDateTime.of(date, LocalTime.MAX);

            //4.调用mapper，查询这一天的订单总数和有效订单数量
            //先查询当天的总的订单数量
            Integer orderCount = orderMapper.getOrederByStatusAndTimeAndStatus(beginTime, endTime, null);
            orderCountList.add(orderCount);
            //在查询当天的订单有效数量
            Integer theNumberOfActiveOrders = orderMapper.getOrederByStatusAndTimeAndStatus(beginTime, endTime, Orders.COMPLETED);
            theNumberOfActiveOrdersList.add(theNumberOfActiveOrders);

        }
        //5.计算订单总数 和 有效订单数, 从开始时间到结束时间之间，计算出订单总数 和 有效订单数
        //订单总数
        Integer totalOrderCount = orderCountList.stream().reduce(Integer::sum).get();
        //有效订单数
        Integer validOrderCount = theNumberOfActiveOrdersList.stream().reduce(Integer::sum).get();

        //订单完成率
        Double orderCompletionRate = 0.0;
        if (totalOrderCount != 0) {
            orderCompletionRate = validOrderCount.doubleValue() / totalOrderCount;
        }

        String dateListStr = StringUtils.join(dateList, ",");
        String orderCountStr = StringUtils.join(orderCountList, ",");
        String validOrderCountStr = StringUtils.join(theNumberOfActiveOrdersList, ",");

        return OrderReportVO.builder()
                .dateList(dateListStr)
                .orderCountList(orderCountStr)
                .validOrderCountList(validOrderCountStr)
                .totalOrderCount(totalOrderCount)
                .validOrderCount(validOrderCount)
                .orderCompletionRate(orderCompletionRate)
                .build();

    }

    /**
     * 销量排名统计
     *
     * @param begin
     * @param end
     * @return
     */
    @Override
    public SalesTop10ReportVO top10(LocalDate begin, LocalDate end) {
        //1.查询指定时间范围，所有订单的销量排名
        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);
        List<GoodsSalesDTO> dtolist = orderMapper.top10(beginTime, endTime);
        //获取名称和数量
        List<String> nameList = dtolist.stream().map(GoodsSalesDTO::getName).collect(Collectors.toList());
        List<Integer> numberList = dtolist.stream().map(GoodsSalesDTO::getNumber).collect(Collectors.toList());
        //组装数据
        String nameListStr = StringUtils.join(nameList, ",");
        String numberListStr = StringUtils.join(numberList, ",");
        //返回数据
        return SalesTop10ReportVO.builder()
                .nameList(nameListStr)
                .numberList(numberListStr)
                .build();
    }

    /**
     * 查询近30天数据
     *
     * @param response
     */
    @Override
    public void exportBusinessData(HttpServletResponse response) {
        //计算出30天前的日期和昨天的日期
        LocalDate begin = LocalDate.now().minusDays(30);
        LocalDate end = LocalDate.now().minusDays(1);
        LocalDateTime beginDateTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endDateTime = LocalDateTime.of(end, LocalTime.MAX);

        BusinessDataVO businessData = workspaceService.getBusinessData(beginDateTime, endDateTime);
        try {
            //读取模板
            FileInputStream fileInputStream = new FileInputStream("D:\\HeiMaJavaEE\\JavaEEExer\\CangQiongTake_Out_Projoct\\sky-take-out\\sky-server\\src\\main\\resources\\templea\\运营数据报表模板.xlsx");
            XSSFWorkbook excel = new XSSFWorkbook(fileInputStream);
            //获取sheet
            XSSFSheet sheet = excel.getSheet("Sheet1");

            //获取第一行并写入日期
            sheet.getRow(1).getCell(1).setCellValue(begin + "至" + end);
            //获取第三行
            //营业额
            sheet.getRow(3).getCell(2).setCellValue(businessData.getTurnover());
            //订单完成率
            sheet.getRow(3).getCell(4).setCellValue(businessData.getOrderCompletionRate());
            //新增用户数
            sheet.getRow(3).getCell(6).setCellValue(businessData.getNewUsers());
            //获取第四行
            //有效订单数
            sheet.getRow(4).getCell(2).setCellValue(businessData.getValidOrderCount());
            //平均客单价
            sheet.getRow(4).getCell(4).setCellValue(businessData.getUnitPrice());

            //循环获取30天的数据
            for (int i = 0; i < 30; i++) {
                //获取当前日期
                LocalDate date = begin.plusDays(i);
                businessData = workspaceService.getBusinessData(LocalDateTime.of(date, LocalTime.MIN), LocalDateTime.of(date, LocalTime.MAX));
                XSSFRow row = sheet.getRow(7 + i);
                //设置日期
                row.getCell(1).setCellValue(date.toString());
                //营业额
                row.getCell(2).setCellValue(businessData.getTurnover());
                //有效订单数
                row.getCell(3).setCellValue(businessData.getValidOrderCount());
                //订单完成率
                row.getCell(4).setCellValue(businessData.getOrderCompletionRate());
                //平均客单价
                row.getCell(5).setCellValue(businessData.getUnitPrice());
                //新增用户数
                row.getCell(6).setCellValue(businessData.getNewUsers());
            }

            //通过输出流将文件下载到客户端浏览器中
            ServletOutputStream outputStream = response.getOutputStream();
            excel.write(outputStream);

            //关闭流
            outputStream.flush();
            outputStream.close();
            fileInputStream.close();
            excel.close();


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
