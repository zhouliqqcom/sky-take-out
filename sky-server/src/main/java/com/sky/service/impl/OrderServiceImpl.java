package com.sky.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.dto.*;
import com.sky.entity.*;
import com.sky.exception.AddressBookBusinessException;
import com.sky.exception.OrderBusinessException;
import com.sky.exception.ShoppingCartBusinessException;
import com.sky.mapper.*;
import com.sky.result.PageResult;
import com.sky.service.OrderService;
import com.sky.utils.HttpClientUtil;
import com.sky.utils.WeChatPayUtil;
import com.sky.vo.OrderPaymentVO;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;
import com.sky.websocket.WebSocketServer;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Lee
 * @created 2024/5/10
 * @describtion
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private AddressBookMapper addressBookMapper;

    @Autowired
    private ShoppingCartMapper shoppingCartMapper;

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderDetailMapper orderDetailMapper;

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private WeChatPayUtil weChatPayUtil;
    @Autowired
    private WebSocketServer webSocketServer;

    @Value("${sky.shop.address}")
    private String shopAddress;

    @Value("${sky.baidu.ak}")
    private String ak;


    /**
     * 用户下单
     *
     * @param dto
     * @return
     */
    @Override
    public OrderSubmitVO submitOrder(OrdersSubmitDTO dto) {
        //把当前的地址簿的ID拿出来去查询详细信息，判断改地址是否存在
        Long addressBookId = dto.getAddressBookId();
        AddressBook addressBook = addressBookMapper.getById(addressBookId);
        if (addressBook == null) {
            //收货地址为空 超出配送范围 购物车为空 抛出异常
            throw new AddressBookBusinessException(MessageConstant.ADDRESS_BOOK_IS_NULL);
        }


        //当前微信登录人的ID
        Long userId = BaseContext.getCurrentId();
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setUserId(userId);
        //查询当前用户的购物车数据车
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.list(shoppingCart);
        if (shoppingCartList == null || shoppingCartList.isEmpty()) {
            throw new ShoppingCartBusinessException(MessageConstant.SHOPPING_CART_IS_NULL);
        }

        //检查用户的收货地址是否超出配送范围
        checkOutOfRange(addressBook.getCityName() + addressBook.getDistrictName() + addressBook.getDetail());
        //构建订单对象
        Orders orders = new Orders();
        BeanUtils.copyProperties(dto, orders);
        orders.setPhone(addressBook.getPhone());
        orders.setAddress(addressBook.getDetail());
        orders.setConsignee(addressBook.getConsignee());
        orders.setNumber(String.valueOf(System.currentTimeMillis()));
        orders.setUserId(userId);
       /* //真实支付代码
        orders.setStatus(Orders.PENDING_PAYMENT);
        orders.setPayStatus(Orders.UN_PAID);*/
        //模拟订单支付
        orders.setStatus(Orders.TO_BE_CONFIRMED);
        orders.setPayStatus(Orders.PAID);

        orders.setOrderTime(LocalDateTime.now());
        //保存订单
        orderMapper.insert(orders);
        //构建订单明细对象  一个订单里面可能会有 多个订单明细
        //遍历购物车列表集合，在把里面的每一个购物车对象转成订单明细对象进行存储
        ArrayList<OrderDetail> orderDetailArrayList = new ArrayList<>();
        for (ShoppingCart cart : shoppingCartList) {
            OrderDetail orderDetail = new OrderDetail();
            BeanUtils.copyProperties(cart, orderDetail);
            orderDetail.setOrderId(orders.getId());
            orderDetailArrayList.add(orderDetail);
        }
        //批量新增订单明细对象
        orderDetailMapper.insertBatch(orderDetailArrayList);

        //一旦订单添加完成，那么则需要删除购物车中的所有数据
        shoppingCartMapper.deleteByUserId(userId);

        //封装返回结果
        OrderSubmitVO orderSubmitVO = OrderSubmitVO.builder()
                .id(orders.getId())
                .orderNumber(orders.getNumber())
                .orderTime(orders.getOrderTime())
                .orderAmount(orders.getAmount())
                .build();

        return orderSubmitVO;
    }


    /**
     * 检查客户的收货地址是否超出配送范围
     *
     * @param address
     */
    private void checkOutOfRange(String address) {
        Map map = new HashMap();
        map.put("address", shopAddress);
        map.put("output", "json");
        map.put("ak", ak);

        //获取店铺的经纬度坐标
        String shopCoordinate = HttpClientUtil.doGet("https://api.map.baidu.com/geocoding/v3", map);

        JSONObject jsonObject = JSON.parseObject(shopCoordinate);
        if (!jsonObject.getString("status").equals("0")) {
            throw new OrderBusinessException("店铺地址解析失败");
        }

        //数据解析
        JSONObject location = jsonObject.getJSONObject("result").getJSONObject("location");
        String lat = location.getString("lat");
        String lng = location.getString("lng");
        //店铺经纬度坐标
        String shopLngLat = lat + "," + lng;

        map.put("address", address);
        //获取用户收货地址的经纬度坐标
        String userCoordinate = HttpClientUtil.doGet("https://api.map.baidu.com/geocoding/v3", map);

        jsonObject = JSON.parseObject(userCoordinate);
        if (!jsonObject.getString("status").equals("0")) {
            throw new OrderBusinessException("收货地址解析失败");
        }

        //数据解析
        location = jsonObject.getJSONObject("result").getJSONObject("location");
        lat = location.getString("lat");
        lng = location.getString("lng");
        //用户收货地址经纬度坐标
        String userLngLat = lat + "," + lng;

        map.put("origin", shopLngLat);
        map.put("destination", userLngLat);
        map.put("steps_info", "0");

        //路线规划
        String json = HttpClientUtil.doGet("https://api.map.baidu.com/directionlite/v1/driving", map);

        jsonObject = JSON.parseObject(json);
        if (!jsonObject.getString("status").equals("0")) {
            throw new OrderBusinessException("配送路线规划失败");
        }

        //数据解析
        JSONObject result = jsonObject.getJSONObject("result");
        JSONArray jsonArray = (JSONArray) result.get("routes");
        Integer distance = (Integer) ((JSONObject) jsonArray.get(0)).get("distance");

        if (distance > 5000) {
            //配送距离超过5000米
            throw new OrderBusinessException("超出配送范围");
        }
    }


    /**
     * 订单支付
     *
     * @param ordersPaymentDTO
     * @return
     */
    @Override
    public OrderPaymentVO payment(OrdersPaymentDTO ordersPaymentDTO) throws Exception {
        // 当前登录用户id
        Long userId = BaseContext.getCurrentId();
        User user = userMapper.getById(userId);

        //调用微信支付接口，生成预支付交易单
        //支付状态
        JSONObject jsonObject = weChatPayUtil.pay(
                //商户订单号
                ordersPaymentDTO.getOrderNumber(),
                //支付金额，单位 元
                new BigDecimal("0.01"),
                //商品描述
                "苍穹外卖订单",
                //微信用户的openid
                user.getOpenid()
        );

        if (jsonObject.getString("code") != null && jsonObject.getString("code").equals("ORDERPAID")) {
            throw new OrderBusinessException("该订单已支付");
        }

        OrderPaymentVO vo = jsonObject.toJavaObject(OrderPaymentVO.class);
        vo.setPackageStr(jsonObject.getString("package"));

        return vo;
    }

    /**
     * * 支付成功，修改订单状态
     *
     * @param outTradeNo
     */
    @Override
    public void paySuccess(String outTradeNo) {
        // 当前登录用户id
        Long userId = BaseContext.getCurrentId();

        // 根据订单号查询当前用户的订单
        Orders ordersDB = orderMapper.getByNumberAndUserId(outTradeNo, userId);

        // 根据订单id更新订单的状态、支付方式、支付状态、结账时间
        Orders orders = Orders.builder()
                .id(ordersDB.getId())
                .status(Orders.TO_BE_CONFIRMED)
                .payStatus(Orders.PAID)
                .checkoutTime(LocalDateTime.now())
                .build();

        orderMapper.update(orders);

        /**
         * 使用WebSocket
         *向商家发送消息进行语音播报
         */
        HashMap<Object, Object> map = new HashMap<>();
        //消息类型，1表示来单提醒
        map.put("type", 1);
        map.put("orderId", orders.getId());
        map.put("content", "订单号：" + orders.getNumber() + "支付成功");
        //向客户端浏览器推送消息
        webSocketServer.sendToAllClient(JSON.toJSONString(map));


    }

    /**
     * 订单搜索
     *
     * @param dto
     * @return
     */
    @Override
    public PageResult conditionSearch(OrdersPageQueryDTO dto) {
        PageHelper.startPage(dto.getPage(), dto.getPageSize());

        Page<Orders> page = orderMapper.pageQuery(dto);

        // 部分订单状态，需要额外返回订单菜品信息，将Orders转化为OrderVO
        //?
        List<OrderVO> orderVOList = getOrderVOList(page);
        long total = page.getTotal();

        return new PageResult(total, orderVOList);
    }


    /**
     * 订单菜品信息
     *
     * @param page
     * @return
     */

    private List<OrderVO> getOrderVOList(Page<Orders> page) {
        //返回订单菜品信息
        ArrayList<OrderVO> orderVOList = new ArrayList<>();

        List<Orders> ordersList = page.getResult();

//        if (!CollectionUtils.isEmpty(ordersList)) {
//            for (Orders orders : ordersList) {
//
//                //将共同的字段复制到OrderVO
//                OrderVO orderVO = new OrderVO();
//                BeanUtils.copyProperties(orders, orderVO);
//                //?
//                String orderDishes = getOrderDishesStr(orders);
//                //将订单菜品信息封装到OrderVO中，并添加到orderVOList
//                orderVO.setOrderDishes(orderDishes);
//                ordersList.add(orderVO);
//            }
//        }
        //并发异常
        Iterator<Orders> iterator = ordersList.iterator();
        while (iterator.hasNext()) {
            Orders orders = iterator.next();
            OrderVO orderVO = new OrderVO();
            BeanUtils.copyProperties(orders, orderVO);
            String orderDishesStr = getOrderDishesStr(orders);
            orderVO.setOrderDishes(orderDishesStr);
            orderVOList.add(orderVO);
        }
        return orderVOList;
    }

    /**
     * 根据订单id获取菜品信息字符串
     *
     * @param orders
     * @return
     */
    private String getOrderDishesStr(Orders orders) {
        //查询订单菜品详情信息（订单中的菜品和数量）
        List<OrderDetail> orderDetailList = orderDetailMapper.getByOrderId(orders.getId());
        //将每一条订单菜品信息拼接为字符串（格式：宫保鸡丁*3；）
        List<String> collect = orderDetailList.stream().map(o -> {
            String orderDish = o.getName() + "*" + o.getNumber() + ";";
            return orderDish;
        }).collect(Collectors.toList());
        //将该订单对应的所有菜品信息拼接在一起
        return String.join("", collect);
    }

    /**
     * 订单查询
     *
     * @param id
     * @return
     */
    @Override
    public OrderVO details(Long id) {
        //根据ID查询订单
        Orders orders = orderMapper.getById(id);
        //查询该订单对应的菜品、套餐明细
        List<OrderDetail> orderDetailList = orderDetailMapper.getByOrderId(orders.getId());
        //将给订单及其菜品明细封装到OrderVO中
        OrderVO orderVO = new OrderVO();
        BeanUtils.copyProperties(orders, orderVO);
        orderVO.setOrderDetailList(orderDetailList);
        return orderVO;
    }

    /**
     * 接单
     *
     * @param dto
     */
    @Override
    public void confirm(OrdersConfirmDTO dto) {
        Orders orders = Orders.builder()
                .id(dto.getId())
                .status(Orders.CONFIRMED)
                .build();

        orderMapper.update(orders);
    }

    /**
     * 拒单
     *
     * @param dto
     */
    @Override
    public void rejection(OrdersRejectionDTO dto) throws Exception {

        //根据ID查询订单
        Orders ordersDB = orderMapper.getById(dto.getId());
        //订单状态判断
        //只有待接单状态下可以拒单
        if (ordersDB == null || !ordersDB.getStatus().equals(Orders.TO_BE_CONFIRMED)) {
            throw new OrderBusinessException(MessageConstant.ORDER_STATUS_ERROR);
        }
        //支付状态
    /*    Integer payStatus = ordersDB.getPayStatus();
        if (payStatus.equals(Orders.PAID)) {
            //如果订单已支付，则需要退款
            //调用微信支付接口，退款
            String refund = weChatPayUtil.refund(
                    //用户订单号
                    ordersDB.getNumber(),
                    //用户退款单号
                    ordersDB.getNumber(),
                    //退款金额，单位 元
                    new BigDecimal("0.01"),
                    //原订单金额
                    ordersDB.getAmount()
            );
        }*/
        //拒单需要退款，根据订单ID更新订单状态、拒单原因、取消时间
        Orders orders = new Orders();
        orders.setId(dto.getId());
        orders.setStatus(Orders.CANCELLED);
        orders.setRejectionReason(dto.getRejectionReason());
        orders.setCancelTime(LocalDateTime.now());

        orderMapper.update(orders);

    }

    /**
     * 取消订单
     *
     * @param dto
     */
    @Override
    public void cancel(OrdersRejectionDTO dto) throws Exception {
        //根据ID查询订单
        Orders ordersDB = orderMapper.getById(dto.getId());

        //支付状态
    /*    Integer payStatus = ordersDB.getPayStatus();
        if (payStatus.equals(Orders.PAID)) {
            //如果订单已支付，则需要退款
            //调用微信支付接口，退款
            String refund = weChatPayUtil.refund(
                    //用户订单号
                    ordersDB.getNumber(),
                    //用户退款单号
                    ordersDB.getNumber(),
                    //退款金额，单位 元
                    new BigDecimal("0.01"),
                    //原订单金额
                    ordersDB.getAmount()
            );
        }*/
        //拒单需要退款
        Orders orders = new Orders();
        orders.setId(dto.getId());
        orders.setStatus(Orders.CANCELLED);
        orders.setRejectionReason(dto.getRejectionReason());
        orders.setCancelTime(LocalDateTime.now());
        orderMapper.update(orders);

    }

    /**
     * 配送订单
     *
     * @param id
     */
    @Override
    public void delivery(Long id) {
        //根据ID查询
        Orders orderDB = orderMapper.getById(id);

        //判断订单是否存在并且订单状态为3
        if (orderDB == null || !orderDB.getStatus().equals(Orders.CONFIRMED)) {
            throw new OrderBusinessException(MessageConstant.ORDER_STATUS_ERROR);
        }
        //更新订单状态，订单状态为派送中
        Orders orders = new Orders();
        orders.setId(orderDB.getId());
        orders.setStatus(Orders.DELIVERY_IN_PROGRESS);
        orderMapper.update(orders);
    }

    /**
     * 完成订单
     *
     * @param id
     */
    @Override
    public void complete(Long id) {
        //根据ID查询
        Orders ordersDB = orderMapper.getById(id);
        //判断订单是否存在并且订单状态为派送中
        if (ordersDB == null || !ordersDB.getStatus().equals(Orders.DELIVERY_IN_PROGRESS)) {
            throw new OrderBusinessException(MessageConstant.ORDER_STATUS_ERROR);
        }
        //更新订单状态，订单状态为完成
        Orders orders = new Orders();
        orders.setId(ordersDB.getId());
        orders.setStatus(Orders.COMPLETED);
        orders.setDeliveryTime(LocalDateTime.now());
        orderMapper.update(orders);
    }

    /**
     * 各个状态的订单数量统计
     *
     * @return
     */
    @Override
    public OrderStatisticsVO statistics() {
        //根据状态，分别查询出待接单、待派送、配送中的订单数量
        Integer toBeConfirmed = orderMapper.countStatus(Orders.TO_BE_CONFIRMED);
        Integer confirm = orderMapper.countStatus(Orders.CONFIRMED);
        Integer deliveInProgress = orderMapper.countStatus(Orders.DELIVERY_IN_PROGRESS);

        //将查询出的数量封装到OrderStatisticsVO中并响应
        OrderStatisticsVO orderStatisticsVO = new OrderStatisticsVO();
        orderStatisticsVO.setToBeConfirmed(toBeConfirmed);
        orderStatisticsVO.setConfirmed(confirm);
        orderStatisticsVO.setDeliveryInProgress(deliveInProgress);
        return orderStatisticsVO;
    }

    /**
     * C端-历史订单查询
     *
     * @param page
     * @param pageSize
     * @param status
     * @return
     */
    @Override
    public PageResult pageQuery4User(int page, int pageSize, Integer status) {
        // 设置分页
        PageHelper.startPage(page, pageSize);
        OrdersPageQueryDTO ordersPageQueryDTO = new OrdersPageQueryDTO();
        ordersPageQueryDTO.setUserId(BaseContext.getCurrentId());
        ordersPageQueryDTO.setStatus(status);
        // 分页条件查询
        Page<Orders> pageNums = orderMapper.pageQuery(ordersPageQueryDTO);
        List<OrderVO> list = new ArrayList();
        // 查询出订单明细，并封装入OrderVO进行响应
        if (pageNums != null && pageNums.getTotal() > 0) {
            for (Orders orders : pageNums) {
                // 订单id
                Long orderId = orders.getId();
                // 查询订单明细
                List<OrderDetail> orderDetails = orderDetailMapper.getByOrderId(orderId);
                // 将订单明细vo集合封装进vo
                OrderVO orderVO = new OrderVO();
                BeanUtils.copyProperties(orders, orderVO);
                orderVO.setOrderDetailList(orderDetails);
                // 封装到vo集合中
                list.add(orderVO);
            }
        }
        return new PageResult(pageNums.getTotal(), list);
    }

    /**
     * 用户取消订单
     *
     * @param id
     */
    @Override
    public void cancelById(Long id) throws Exception {
        Orders orderDB = orderMapper.getById(id);
        //判断订单是否存在
        if (orderDB == null) {
            throw new OrderBusinessException(MessageConstant.ORDER_NOT_FOUND);
        }
        //判断订单状态
        if (orderDB.getStatus().equals(Orders.CANCELLED)) {
            throw new OrderBusinessException(MessageConstant.ORDER_STATUS_ERROR);
        }
        //更新订单状态，订单状态为已取消
        Orders orders = new Orders();
        //设置订单状态
        orders.setId(orderDB.getId());
        //如果订单状态为待接单，则需要退款
        if (orderDB.getStatus().equals(Orders.TO_BE_CONFIRMED)) {
            /*weChatPayUtil.refund(
                    //用户订单号
                    orderDB.getNumber(),
                    //用户退款单号
                    orderDB.getNumber(),
                    //退款金额，单位 元
                    new BigDecimal("0.01"),
                    //原订单金额
                    orderDB.getAmount()
            );*/
            //支付状态修改为退款
            orders.setPayStatus(Orders.REFUND);
        }
        //更新订单状态，取消原因，取消时间
        orders.setStatus(Orders.CANCELLED);
        orders.setCancelReason(MessageConstant.CANCELLATION_OF_THE_USER_ORDER);
        orders.setCancelTime(LocalDateTime.now());
        orderMapper.update(orders);
    }

    /**
     * 再来一单
     *
     * @param id
     */
    @Override
    public void reptition(Long id) {
        //获取用户id
        Long userId = BaseContext.getCurrentId();
        //根据订单id查询订单明细
        List<OrderDetail> orderDetailList = orderDetailMapper.getByOrderId(id);
        //封装成购物车数据
        List<ShoppingCart> shoppingCartList = orderDetailList.stream().map(o -> {
            ShoppingCart shoppingCart = new ShoppingCart();
            //拷贝
            BeanUtils.copyProperties(o, shoppingCart, "id");
            shoppingCart.setUserId(userId);
            shoppingCart.setCreateTime(LocalDateTime.now());
            return shoppingCart;
        }).collect(Collectors.toList());
        //将购物车数据插入到数据库
        shoppingCartMapper.insertBatch(shoppingCartList);
    }

    /**
     *
     * @param id
     */
    @Override
    public void reminder(Long id) {
        Orders orders = orderMapper.getById(id);
        //判断订单是否存在
        if (orders == null) {
            throw new OrderBusinessException(MessageConstant.ORDER_NOT_FOUND);
        }
        //基于websocketServer实现催单
        HashMap<Object, Object> map = new HashMap<>();
        //消息类型，1表示来单提醒
        map.put("type", 2);
        map.put("orderId", orders.getId());
        map.put("content", "订单号：" + orders.getNumber() + "支付成功");
        //向客户端浏览器推送消息
        webSocketServer.sendToAllClient(JSON.toJSONString(map));

    }


}
