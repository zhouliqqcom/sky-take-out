package com.sky.service;

import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.result.PageResult;

public interface EmployeeService {

    /**
     * 员工登录
     *
     * @param employeeLoginDTO
     * @return
     */
    Employee login(EmployeeLoginDTO employeeLoginDTO);

    /**
     * 新增员工
     *
     * @param dto
     */
    void saveEmp(EmployeeDTO dto);

    /**
     * 员工分页查询
     *
     * @param dto
     * @return
     */
    PageResult page(EmployeePageQueryDTO dto);

    /**
     * 员工账号的启用和禁用
     *
     * @param status
     * @param id
     */

    void enableDisable(Integer status, Long id);

    /**
     * 根据id查询员工信息
     *
     * @param id
     * @return
     */
    Employee getById(Long id);

    /**
     * 修改员工信息
     *
     * @param dto
     */
    void update(EmployeeDTO dto);
}
