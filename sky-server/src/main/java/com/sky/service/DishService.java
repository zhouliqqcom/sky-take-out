package com.sky.service;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.vo.DishVO;

import java.util.List;

/**
 * @author Lee
 * @created 2024/5/3
 * @describtion
 */
public interface DishService {

    /**
     * 新增菜品
     *
     * @param dto
     */
    void saveDish(DishDTO dto);

    /**
     * 分页查询
     *
     * @param dto
     * @return
     */
    PageResult page(DishPageQueryDTO dto);

    /**
     * 菜品删除
     *
     * @param ids
     */
    void delecDishByIds(List<Long> ids);

    /**
     * 根据id查询菜品信息
     *
     * @param id
     * @return
     */
    DishVO getByIdwithFlavor(Long id);

    /**
     * 修改菜品
     * @param dto
     */
    void updateWithFlavor(DishDTO dto);

    /**
     * 菜品起售和停售
     * @param status
     * @param id
     */
    void startsAndDiscontinues(Integer status, Long id);

    /**
     * 根据分类id查询菜品
     * @param categoryId
     * @return
     */
    List<Dish> list(Long categoryId,String name);

    /**
     * 条件查询菜品和口味
     * @param dish
     * @return
     */
    List<DishVO> listWithFlavor(Dish dish);
}
