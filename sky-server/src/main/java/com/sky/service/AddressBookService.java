package com.sky.service;

import com.sky.entity.AddressBook;

import java.util.List;

/**
 * @author Lee
 * @created 2024/5/10
 * @describtion
 */
public interface AddressBookService {
    /**
     * 新增地址
     *
     * @param addressBook
     */
    void save(AddressBook addressBook);

    /**
     * 查询登录用户所有地址
     *
     * @param addressBook
     * @return
     */
    List<AddressBook> list(AddressBook addressBook);

    /**
     * 根据ID查询地址
     *
     * @param id
     * @return
     */
    AddressBook getById(Long id);

    /**
     * 根据ID删除地址
     *
     * @param id
     */
    void delById(Long id);

    /**
     * 修改地址
     *
     * @param addressBook
     */
    void update(AddressBook addressBook);

    /**
     * 设置默认地址
     *
     * @param addressBook
     */
    void setDefault(AddressBook addressBook);
}
