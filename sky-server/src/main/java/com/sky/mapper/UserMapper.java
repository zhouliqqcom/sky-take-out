package com.sky.mapper;

import com.sky.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.HashMap;

/**
 * @author Lee
 * @created 2024/5/7
 * @describtion
 */
@Mapper
public interface UserMapper {
    /**
     * 根据openid查询用户
     *
     * @param openId
     * @return
     */
    @Select("select * from user where openid = #{openid}")
    User getByOpenid(String openId);

    /**
     * 插入数据
     *
     * @param user
     */
    void insert(User user);

    @Select("select * from user where id = #{userId}")
    User getById(Long userId);

    /**
     *
     * @param map
     * @return
     */
    Integer countByMap(HashMap<Object, Object> map);
}
