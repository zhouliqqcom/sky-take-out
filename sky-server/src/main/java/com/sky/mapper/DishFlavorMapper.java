package com.sky.mapper;

import com.sky.entity.DishFlavor;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author Lee
 * @created 2024/5/3
 * @describtion
 */
@Mapper
public interface DishFlavorMapper {
    /**
     * 添加口味
     *
     * @param flavors
     */
    void insertBatch(List<DishFlavor> flavors);

    /**
     * 根据菜品id删除对应的口味数据
     * @param id
     */
    @Delete("delete  from dish_flavor where dish_id = #{id}")
    void deleteDishFlavorByDishId(Long id);

    /**
     * 根据菜品id查询对应的口味数据
     * @param id
     * @return
     */
    @Select("select * from dish_flavor where dish_id = #{id}")
    List<DishFlavor> getFlavorById(Long id);
}
