package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.anno.AutoFill;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author Lee
 * @created 2024/5/2
 * @describtion
 */
@Mapper
public interface CategoryMapper {


    /**
     * 分页分类查询
     *
     * @param dto
     * @return
     */
    Page<Category> page(CategoryPageQueryDTO dto);

    /**
     * 新增分类
     *
     * @param category
     */
    @AutoFill(OperationType.INSERT)
    void save(Category category);

    /**
     * 修改分类
     * @param build
     */
    @AutoFill(OperationType.UPDATE)
    void update(Category build);

    /**
     * 查询类型
     *
     * @param type
     * @return
     */
    List<Category> typeQueryList(Integer type);

    /**
     * 根据ID删除分类
     *
     * @param id
     */
    void delById(Long id);
}
