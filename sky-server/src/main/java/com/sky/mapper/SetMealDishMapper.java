package com.sky.mapper;

import com.sky.entity.SetmealDish;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Lee
 * @created 2024/5/3
 * @describtion
 */
@Mapper
public interface SetMealDishMapper {

    /**
     * 根据菜品id查询对应的套餐id
     *
     * @param ids
     * @return
     */

    List<Long> getSetMealByDishId(List<Long> ids);

    /**
     * 批量保存套餐和菜品的关联关系
     *
     * @param setmealDishes
     */
    void insertBatch(List<SetmealDish> setmealDishes);

    /**
     * 根据ID删除
     *
     * @param id
     */
    @Delete("delete from setmeal_dish where setmeal_id = #{setmealId}")
    void deleteById(Long id);
}
