package com.sky.mapper;

import com.sky.entity.AddressBook;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @author Lee
 * @created 2024/5/10
 * @describtion
 */
@Mapper
public interface AddressBookMapper {
    /**
     * 新增地址
     *
     * @param addressBook
     */
    void insert(AddressBook addressBook);

    /**
     * 查询登录用户所有地址
     *
     * @param addressBook
     * @return
     */
    List<AddressBook> list(AddressBook addressBook);

    /**
     * 根据ID查询地址
     *
     * @param id
     * @return
     */
    @Select("select  * from address_book where id = #{id}")
    AddressBook getById(Long id);

    /**
     * 根据ID删除地址
     *
     * @param id
     */
    @Delete("delete from address_book where id = #{id}")
    void delById(Long id);

    /**
     * 修改地址
     *
     * @param addressBook
     */
    void update(AddressBook addressBook);

    /**
     * 根据 用户id修改 是否默认地址
     *
     * @param addressBook
     */
    @Update("update   address_book set  is_default = #{isDefault} where user_id = #{userId}")
    void updateIsDefaultByUserId(AddressBook addressBook);
}
