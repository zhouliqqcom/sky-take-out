package com.sky.mapper;


import com.github.pagehelper.Page;
import com.sky.dto.GoodsSalesDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.entity.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Lee
 * @created 2024/5/10
 * @describtion
 */
@Mapper
public interface OrderMapper {
    /**
     * 插入订单数据
     *
     * @param order
     */
    void insert(Orders order);

    /**
     * 根据订单号和用户id查询订单
     *
     * @param orderNumber
     * @param userId
     */
    @Select("select * from orders where number = #{orderNumber} and user_id= #{userId}")
    Orders getByNumberAndUserId(String orderNumber, Long userId);

    /**
     * 修改订单信息
     *
     * @param orders
     */
    void update(Orders orders);

    /**
     * 订单搜索
     *
     * @param dto
     * @return
     */
    Page<Orders> pageQuery(OrdersPageQueryDTO dto);

    @Select("select  * from orders where  id = #{id}")
    Orders getById(Long id);

    /**
     * 根据状态统计订单数量
     *
     * @param status
     */
    @Select("select count(id) from orders where status = #{status}")
    Integer countStatus(Integer status);

    /**
     * 用户下单 超时订单
     *
     * @param status
     * @param time
     * @return
     */
    @Select("select * from orders where status = #{status} and  order_time < #{time}")
    List<Orders> getOrederByStatusAndTime(Integer status, LocalDateTime time);

    /**
     * 营业额数据统计
     *
     * @param map
     * @return
     */
    Double sumByMap(HashMap<Object, Object> map);

    /**
     * 订单总数和有效订单数量
     *
     * @param beginTime
     * @param endTime
     * @param status
     * @return
     */
    Integer getOrederByStatusAndTimeAndStatus(LocalDateTime beginTime, LocalDateTime endTime, Integer status);

    /**
     * 销量排名统计
     * @param beginTime
     * @param endTime
     * @return
     */
    List<GoodsSalesDTO> top10(LocalDateTime beginTime, LocalDateTime endTime);

    /**
     * 根据动态条件统计订单数量
     * @param map
     * @return
     */
    Integer countByMap(Map map);
}
