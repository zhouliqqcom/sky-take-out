package com.sky.controller.user;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;
import com.sky.result.Result;
import com.sky.service.ShoppingCartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Lee
 * @created 2024/5/9
 * @describtion
 */
@RestController
@RequestMapping("/user/shoppingCart")
@Slf4j
@Api(tags = "C端-购物车接口")
public class ShoppingCartController {

    @Autowired
    private ShoppingCartService shoppingCartService;

    /**
     * 添加购物车
     *
     * @param dto
     * @return
     */
    @PostMapping("/add")
    @ApiOperation("添加购物车")
    public Result saveCart(@RequestBody ShoppingCartDTO dto) {
        log.info("添加购物车：{}", dto);
        shoppingCartService.saveCart(dto);
        return Result.success();
    }

    /**
     * @return
     */
    @GetMapping("/list")
    @ApiOperation("查询购物车")
    public Result<List<ShoppingCart>> list() {
        List<ShoppingCart> list = shoppingCartService.list();
        return Result.success(list);
    }

    /**
     * 清空购物车商品
     *
     * @return
     */
    @DeleteMapping("/clean")
    @ApiOperation("清空购物车商品")
    public Result clean() {
        shoppingCartService.clean();
        return Result.success();
    }

    /**
     * 减少购物车商品
     * @param dto
     * @return
     */
    @PostMapping("/sub")
    @ApiOperation("减少购物车商品")
    public Result delShoppingCart(@RequestBody ShoppingCartDTO dto) {
        log.info("删除购物车中一个商品，商品：{}", dto);
        shoppingCartService.subShoppingCart(dto);
        return Result.success();

    }

}
