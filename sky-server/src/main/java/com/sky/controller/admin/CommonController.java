package com.sky.controller.admin;

import com.sky.result.Result;
import com.sky.utils.AliOssUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

/**
 * @author Lee
 * @created 2024/5/3
 * @describtion
 */
@RestController
@RequestMapping("/admin/common")
@Api(tags = "通用接口")
@Slf4j
public class CommonController {

    @Autowired
    private AliOssUtil aliOssUtil;

    /**
     * 图片上传
     *
     * @param file 文件
     * @return
     * @throws IOException
     */
    @PostMapping("/upload")
    @ApiOperation("文件上传")
    public Result upload(MultipartFile file) throws IOException {
        log.info("文件上传：{}", file);
        //获取原始文件名
        String originalFilename = file.getOriginalFilename();

        // 产生aavavasvass
        String fileName = UUID.randomUUID().toString();

        //获取原始文件后缀名
        int index = originalFilename.lastIndexOf(".");
        String end = originalFilename.substring(index);

        //在上传图片到oss上，最后返回该图片的网络绝对路径
        byte[] bytes = file.getBytes();
        String url = aliOssUtil.upload(bytes, fileName + end);
        return Result.success(url);
    }

}
