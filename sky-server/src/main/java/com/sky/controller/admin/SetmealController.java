package com.sky.controller.admin;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.vo.SetmealVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Lee
 * @created 2024/5/4
 * @describtion
 */
@RestController
@RequestMapping("/admin/setmeal")
@Slf4j
@Api(tags = "套餐修改信息")
public class SetmealController {

    @Autowired
    private SetmealService setmealService;

    /**
     * 新增套餐
     *
     * @param dto
     * @return
     */
    @PostMapping
    @ApiOperation("新增套餐")
    @CacheEvict(cacheNames = "setmealCache", key = "#dto.categoryId")
    public Result saveSetmeal(@RequestBody SetmealDTO dto) {
        setmealService.saveSetmeal(dto);
        return Result.success();
    }

    /**
     * 分页查询
     *
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("分页查询")
    public Result<PageResult> page(SetmealPageQueryDTO pageQueryDTO) {
        PageResult pageResult = setmealService.page(pageQueryDTO);
        return Result.success(pageResult);
    }

    /**
     * 批量删除套餐
     *
     * @param ids
     * @return
     */
    @DeleteMapping
    @ApiOperation("批量删除")
    @CacheEvict(cacheNames = "setmealCache", allEntries = true)
    public Result delSetmeal(@RequestParam List<Long> ids) {
        setmealService.delSetmeal(ids);
        return Result.success();

    }

    /**
     * 根据ID查询套餐
     *
     * @return
     */
    @GetMapping("/{id}")
    @ApiOperation("根据ID查询套餐")
    public Result<SetmealVO> getByIdSetmeal(@PathVariable Long id) {
        SetmealVO setmealVO = setmealService.getByIdSetmeal(id);
        return Result.success(setmealVO);
    }

    /**
     * 修改套餐
     *
     * @param dto
     * @return
     */
    @PutMapping
    @ApiOperation("修改套餐")
    @CacheEvict(cacheNames = "setmealCache", allEntries = true)
    public Result updateSetmeal(@RequestBody SetmealDTO dto) {
        setmealService.update(dto);
        return Result.success();
    }


    /**
     * 套餐起售和停售
     *
     * @return
     */
    @PostMapping("/status/{status}")
    @ApiOperation("套餐起售和停售")
    @CacheEvict(cacheNames = "setmealCache", allEntries = true)
    public Result startsAndDiscontinues(@PathVariable Integer status, Long id) {
        setmealService.startsAndDiscontinues(status, id);
        return Result.success();

    }

}
