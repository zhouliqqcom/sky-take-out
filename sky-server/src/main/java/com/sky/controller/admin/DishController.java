package com.sky.controller.admin;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * @author Lee
 * @created 2024/5/3
 * @describtion
 */
@RestController
@RequestMapping("/admin/dish")
@Api(tags = "菜品相关接口")
@Slf4j
public class DishController {

    @Autowired
    private DishService dishService;
    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * 清理缓存数据
     *
     * @param pattern
     */
    private void cleanCache(String pattern) {
        Set keys = redisTemplate.keys(pattern);
        redisTemplate.delete(keys);
    }

    /**
     * 新增菜品
     *
     * @param dto
     * @return
     */
    @PostMapping
    @ApiOperation("新增菜品")
    public Result save(@RequestBody DishDTO dto) {
        dishService.saveDish(dto);

        //清理缓存数据
        String key = "dish_" + dto.getCategoryId();
        cleanCache(key);

        return Result.success();
    }

    /**
     * 菜品分页查询
     *
     * @param dto
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("菜品分页查询")
    public Result page(DishPageQueryDTO dto) {
        PageResult pageResult = dishService.page(dto);
        return Result.success(pageResult);
    }

    /**
     * 菜品删除
     *
     * @param ids
     * @return
     */
    @DeleteMapping
    @ApiOperation("菜品批量删除")
    public Result delecDishByIds(@RequestParam List<Long> ids) {
        dishService.delecDishByIds(ids);
        //清理缓存数据
        cleanCache("dish_*");
        return Result.success();

    }

    /**
     * 根据ID查询菜品
     *
     * @return
     */
    @GetMapping("/{id}")
    @ApiOperation("根据ID查询菜品")
    public Result<DishVO> getById(@PathVariable Long id) {
        DishVO dishVO = dishService.getByIdwithFlavor(id);
        return Result.success(dishVO);

    }

    /**
     * 修改菜品
     *
     * @param dto
     * @return
     */
    @PutMapping
    @ApiOperation("修改菜品")
    public Result update(@RequestBody DishDTO dto) {
        dishService.updateWithFlavor(dto);

        //清理缓存数据
        cleanCache("dish_" + dto.getCategoryId());

        return Result.success();
    }

    /**
     * 菜品起售和停售
     *
     * @param status 状态
     * @param id     菜品id
     * @return
     */
    @PostMapping("/status/{status}")
    @ApiOperation("起售和停售菜品")
    public Result startsAndDiscontinues(@PathVariable Integer status, Long id) {
        dishService.startsAndDiscontinues(status, id);
        //清理缓存数据
        cleanCache("dish_*");
        return Result.success();
    }

    /**
     * 根据分类id查询菜品
     *
     * @param categoryId
     * @return
     */
    @GetMapping("/list")
    @ApiOperation("根据分类id查询菜品")
    public Result<List<Dish>> list(Long categoryId, String name) {
        List<Dish> list = dishService.list(categoryId, name);
        return Result.success(list);
    }

}
