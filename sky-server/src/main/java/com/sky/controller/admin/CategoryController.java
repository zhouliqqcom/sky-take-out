package com.sky.controller.admin;

import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Lee
 * @created 2024/5/2
 * @describtion
 */
@RestController("adminCategoryController")
@RequestMapping("/admin/category")
@Slf4j
@Api(tags = "分类管理相关接口")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 分类分页查询
     *
     * @param dto
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("分类分页查询")
    public Result<PageResult> page(CategoryPageQueryDTO dto) {
        PageResult pageResult = categoryService.page(dto);
        return Result.success(pageResult);
    }

    /**
     * 新增分类
     *
     * @param dto
     * @return
     */
    @PostMapping
    @ApiOperation("新增分类")
    public Result save(@RequestBody CategoryDTO dto) {
        categoryService.save(dto);
        return Result.success();
    }

    /**
     * 分类的启用和禁用
     *
     * @param status 状态
     * @param id     分类ID
     * @return
     */
    @PostMapping("/status/{status}")
    @ApiOperation("禁用启用分类")
    public Result enableDisable(@PathVariable Integer status, Long id) {
        categoryService.enableDisable(status, id);
        return Result.success();
    }

    /**
     * 根据类型查询分类
     *
     * @param type 类型
     * @return List集合
     */
    @GetMapping("/list")
    @ApiOperation("类型分类查询")
    public Result<List<Category>> typeQueryList(Integer type) {
        List<Category> categoryList = categoryService.typeQueryList(type);
        return Result.success(categoryList);
    }

    /**
     * 修改分类
     *
     * @param dto
     * @return
     */

    @PutMapping
    @ApiOperation("修改分类")
    public Result update(@RequestBody CategoryDTO dto) {
        categoryService.update(dto);
        return Result.success();
    }

    /**
     * 根据id删除分类
     *
     * @param id
     * @return
     */

    @DeleteMapping
    @ApiOperation("根据id删除分类")
    public Result delById(Long id) {
        categoryService.delById(id);
        return Result.success();

    }

}
