package com.sky.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 新增员工
 */
@Data
public class EmployeeDTO implements Serializable {
    /**
     * 此时的id在新增员工时用不到，在修改员工时才可以用到
     */
    private Long id;

    private String username;

    private String name;

    private String phone;

    private String sex;

    private String idNumber;

}
